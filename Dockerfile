FROM node:latest

COPY ./build/ /app/build/

RUN npm install -g serve

WORKDIR /app

CMD ["serve", "-s", "build", "-l", "3000"]
