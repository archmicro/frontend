// Created with create-react-app (https://github.com/facebook/create-react-app)
import { 
  MemoryRouter as Router, Routes, Route 
} from "react-router-dom"
import { useState, useEffect } from "react"
import axios from "axios"

//const backendURL = "http://127.0.0.1:8080"
//await axios.get(`${backendURL}/api/test`)

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Main />} />
      </Routes>
    </Router>
  )
}

function Main() {
  const [fromDB, setFromDB] = useState([])
  const [messages, setMessages] = useState([])
  const [newEntry, setNewEntry] = useState("")

  const handlePost = async () => {
    try {
      if (!newEntry) {
        const message = "Cannot be empty"
        setMessages((prevMessages) => [...prevMessages, message])
        setTimeout(() => {
          setMessages(
            (prevMessages) => prevMessages.filter((m) => m !== message)
          )
        }, 5000)
        return
      }
      await axios.post("http://api.site.local/post", { Content: newEntry })
      setNewEntry("")
    } catch (error) {
      console.error(error)
    }
  }

  const handleDeleteAll = async () => {
    try {
      await axios.delete('http://api.site.local/delete-all')
      setFromDB([])
    } catch (error) {
      console.error(error)
    }
  }

  const handleRunHighLoad = async () => {
    try {
      await axios.get(`http://api.site.local/run-highload`)
    } catch (error) {
      console.error(error)
    }
  }

  useEffect(() => {
    const socket = new WebSocket('ws://api.site.local/messages')
    socket.onopen = () => {
      console.log('WebSocket connection opened')
    }
    socket.onclose = () => {
      console.log('WebSocket connection closed')
    }
    socket.onmessage = (event) => {
      const message = event.data
      setMessages((prevMessages) => [...prevMessages, message])
      setTimeout(() => {
        setMessages(
          (prevMessages) => prevMessages.filter((m) => m !== message)
        )
      }, 5000)
    }
    return () => {
      socket.close()
    }
  }, [])

  useEffect(() => {
    const socket2 = new WebSocket('ws://api.site.local/get')
    socket2.onopen = () => {
      console.log('WebSocket connection opened')
    }
    socket2.onclose = () => {
      console.log('WebSocket connection closed')
    }
    socket2.onmessage = (event) => {
      setFromDB(JSON.parse(event.data))
    }
    return () => {
      socket2.close()
    }
  }, [])

  return (
    <div>
      <h4>Messages:</h4>
      <ul>
        {messages.map((message, index) => (
          <li key={index}>{message}</li>
        ))}
      </ul>
      <h4>Highload:</h4>
      <button onClick={handleRunHighLoad}>Run</button><br/>
      <a href="http://api.site.local/get-highload">Results</a>
      <h4>Data:</h4>
      <input
        type="text"
        value={newEntry}
        onChange={(event) => setNewEntry(event.target.value)}
      />
      <button onClick={handlePost}>Post</button>
      <button onClick={handleDeleteAll}>Delete All</button>
      <ul>
        {fromDB.map((entry, index) => (
          <li key={index}>{entry.Content}</li>
        ))}
      </ul>
    </div>
  )
}

export default App
